package multiobj;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloObjective;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplexMultiCriterionExpr;

/**
 * MILP provides a multiobjective mixed integer linear programming model
 * for a patient assignment problem.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MILP implements AutoCloseable {
  private final Problem problem;           // the problem to solve
  private final int nProviders;            // number of providers
  private final int nPatients;             // number of patients
  private final IloCplex mip;              // the MIP model
  private final IloIntVar[][] assign;      // assign[i][j] is 1 if patient i is
                                           // assigned to provider j
  private final IloNumVar maxDist;         // maximum patient travel distance
  private final IloLinearNumExpr avgDist;  // average patient travel distance
  private final IloNumVar maxLoad;         // maximum fraction provider load
  private final IloNumVar minLoad;         // minimum fraction provider load
  private final IloNumVar[] load;          // fractional load on each provider
  private IloObjective objective;          // the current objective function

  /**
   * Constructor that builds the MILP model (other than the objective).
   * @param prob the problem to solve
   * @throws IloException if CPLEX cannot build part of the model
   */
  public MILP(final Problem prob) throws IloException {
    problem = prob;
    nProviders = problem.getNProviders();
    nPatients = problem.getNPatients();
    // Create the CPLEX instance.
    mip = new IloCplex();
    // Create the variables.
    assign = new IloIntVar[nPatients][nProviders];
    double maxDistance = problem.getMaxDistance();
    for (int i = 0; i < nPatients; i++) {
      for (int j = 0; j < nProviders; j++) {
        assign[i][j] = mip.boolVar("Assign_" + i + "_to_" + j);
        // Block any assignment that violates the maximum distance rule.
        if (problem.getDistance(i, j) > maxDistance) {
          assign[i][j].setUB(0);
        }
      }
    }
    maxDist = mip.numVar(0, maxDistance, "Max_travel");
    maxLoad = mip.numVar(0, 1, "Max_load");
    minLoad = mip.numVar(0, 1, "Min_load");
    load = new IloNumVar[nProviders];
    for (int j = 0; j < nProviders; j++) {
      load[j] = mip.numVar(0, 1, "Load_" + j);
    }
    // Constrain each patient to be assigned exactly once.
    for (int i = 0; i < nPatients; i++) {
      mip.addEq(mip.sum(assign[i]), 1.0, "Assign_" + i + "_once");
    }
    // Define maximum travel distance.
    for (int i = 0; i < nPatients; i++) {
      for (int j = 0; j < nProviders; j++) {
        mip.addLe(mip.prod(problem.getDistance(i, j), assign[i][j]), maxDist,
                  "max_dist_" + i + "_" + j);
      }
    }
    // Define provider loads.
    for (int j = 0; j < nProviders; j++) {
      double c = problem.getCapacity(j);
      double f = 1.0 / c;
      IloLinearNumExpr sum = mip.linearNumExpr();
      for (int i = 0; i < nPatients; i++) {
        sum.addTerm(f, assign[i][j]);
      }
      mip.addEq(sum, load[j], "Load_" + j);
    }
    // Define the max and min load variables.
    for (int j = 0; j < nProviders; j++) {
      mip.addLe(load[j], maxLoad, "Max_load_" + j);
      mip.addGe(load[j], minLoad, "Min_load_" + j);
    }
    // Set up an objective expression for average distance traveled.
    avgDist = mip.linearNumExpr();
    double f = 1.0 / problem.getNPatients();
    for (int i = 0; i < nPatients; i++) {
      for (int j = 0; j < nProviders; j++) {
        avgDist.addTerm(f * problem.getDistance(i, j), assign[i][j]);
      }
    }
  }

  /**
   * Tries to solve the model.
   * @param tl time limit (per priority level?) in seconds
   * @param aTol vector of absolute tolerances, in the order of the Objective
   * enumeration, or null
   * @param rTol vector of relative tolerances, in the order of the Objective
   * enumeration, or null
   * @return the final status
   * @throws IloException if CPLEX runs into trouble
   */
  public IloCplex.Status solve(final double tl, final double[] aTol,
                               final double[] rTol)
                         throws IloException {
    // Build the objective function.
    buildObjectiveFunction(aTol, rTol);
    // Set the time limit.
    mip.setParam(IloCplex.DoubleParam.TimeLimit, tl);
    // Try to solve the model.
    mip.solve();
    System.out.println("Solver did " + mip.getMultiObjNsolves() + " solves.");
    return mip.getStatus();
  }

  /**
   * Closes the model.
   */
  @Override
  public void close()  {
    mip.close();
  }

  /**
   * Gets the variable or expression corresponding to a particular objective.
   * @param obj the objective
   * @return the corresponding variable
   * @throws IloException if there is no such variable
   */
  private IloNumExpr getObj(final Objective obj) throws IloException {
    switch (obj) {
      case AVGDISTANCE:
        return avgDist;
      case MAXDISTANCE:
        return maxDist;
      case MAXUTIL:
        return maxLoad;
      case MINUTIL:
        return minLoad;
      default:
        throw new IloException("No such objective: " + obj);
    }
  }

  /**
   * Solves four models to get optimal solutions for each individual objective.
   * @return the optimal objective values, in the order average distance,
   * maximum distance, spread, maximum load.
   * @throws IloException if any solution attempt fails
   */
  public double[] getUtopia() throws IloException {
    Objective[] objs = Objective.values();
    double[] utopia = new double[objs.length];
    // Suppress CPLEX log output.
    mip.setOut(null);
    IloCplex.Status status;
    // Solve for each objective.
    for (int i = 0; i < objs.length; i++) {
      // Drop any previous objective.
      dropObjective();
      // Set a new objective.
      IloNumExpr obj = getObj(objs[i]);
      objective = objs[i].isMinimized() ? mip.addMinimize(obj)
                                        : mip.addMaximize(obj);
      // Try to solve the model.
      mip.solve();
      status = mip.getStatus();
      // If successful, record the objective value.
      if (status == IloCplex.Status.Optimal) {
        utopia[i] = mip.getObjValue();
      } else {
        throw new IloException("Could not optimize " + objs[i]
                               + "; status = " + status + ".");
      }
    }
    // Remove the last objective function used and restore the log output.
    dropObjective();
    mip.setOut(System.out);
    return utopia;
  }

  /**
   * Removes any existing objective function.
   * @throws IloException if the objective cannot be removed
   */
  private void dropObjective() throws IloException {
    if (objective != null) {
      mip.remove(objective);
    }
  }

  /**
   * Adds a multicriterion objective function to the model.
   * @param aTol a vector of absolute tolerances (or null)
   * @param rTol a vector of relative tolerances (or null)
   * @throws IloException if CPLEX cannot add the objective function
   */
  private void buildObjectiveFunction(final double[] aTol,
                                      final double[] rTol)
               throws IloException {
    // Drop any current objective function.
    dropObjective();
    // Build an array of objective expressions in descending priority order.
    IloNumExpr[] criteria
      = new IloNumExpr[] {avgDist, maxDist, maxLoad, minLoad};
    // All criteria get weight one except minimum load, which gets weight -1
    // since it is being maximized within a minimization problem (and has equal
    // weight to its priority-level sibling maximum load).
    double[] weight = new double[] {1, 1, 1, -1};
    // Priorities are 2 (highest) for average distance, 1 (medium) for longest
    // distance, and 0 (lowest) for provider utilization measures.
    int[] priority = new int[] {2, 1, 0, 0};
    IloCplexMultiCriterionExpr obj =
      mip.staticLex(criteria, weight, priority, aTol, rTol,
                    "Combined Objective");
    // Add the objective.
    // Note that, for some reason, addMinimize() cannot be used; add() and
    // minimize() must be chained.
    objective = mip.minimize(obj);
    mip.add(objective);
  }

  /**
   * Gets the final values of the various criteria.
   * @return the criteria values
   * @throws IloException if the solution cannot be recovered
   */
  public double[] getResults() throws IloException {
    Objective[] objs = Objective.values();
    double[] results = new double[objs.length];
    for (int i = 0; i < objs.length; i++) {
      IloNumExpr v = getObj(objs[i]);
      results[i] = mip.getValue(v);
    }
    return results;
  }

  /**
   * Provides a summary of one of the objective functions.
   * @param index the index of the objective function
   * @return a string summarizing the result for that objective
   * @throws IloException if any part of the summary cannot be obtained
   */
  public String summary(final int index) throws IloException {
    StringBuilder sb = new StringBuilder();
    sb.append("Status = ")
      .append(mip.getMultiObjInfo(
                    IloCplex.MultiObjIntInfo.MultiObjStatus, index))
      .append(", value = ")
      .append(Problem.format(
                mip.getMultiObjInfo(
                      IloCplex.MultiObjNumInfo.MultiObjObjValue, index)))
      .append(", bound = ")
      .append(Problem.format(
                mip.getMultiObjInfo(
                      IloCplex.MultiObjNumInfo.MultiObjBestObjValue, index)))
      .append(".");
    return sb.toString();
  }

  /**
   * Sets the log output level.
   * @param level the new level
   * @throws IloException if CPLEX balks
   */
  public void setLogOutput(final int level) throws IloException {
    mip.setParam(IloCplex.Param.MultiObjective.Display, 2);
  }
}
