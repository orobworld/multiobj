package multiobj;

/**
 * Objective enumerates the possible objective functions.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public enum Objective {
  /** AVGDISTANCE is the average distance traveled by a patient. */
  AVGDISTANCE("average distance traveled", true),
  /** MAXDISTANCE is the maximum distance traveled by a patient. */
  MAXDISTANCE("longest distance traveled", true),
  /** MAXUTIL is the maximum utilization (as a fraction of capacity) of
   * any provider. */
  MAXUTIL("maximum provider utilization", true),
  /** SPREAD is the difference between highest and lowest provider utilization
   * (as a fraction of respective capacities). */
  MINUTIL("minimum provider utilization", false);

  private final String label;      // a label to use in prints
  private final boolean minimize;  // minimize if true, maximize if false

  /**
   * Constructor.
   * @param lbl the label for the enumeration value
   * @param min if true, minimize; if false, maximize
   */
  Objective(final String lbl, final boolean min) {
    label = lbl;
    minimize = min;
  }

  /**
   * Returns a label for the choice.
   * @return the label to use
   */
  @Override
  public String toString() {
    return label;
  }

  /**
   * Checks if the objective should be minimized.
   * @return true if minimize, false if maximized
   */
  public boolean isMinimized() {
    return minimize;
  }

}
