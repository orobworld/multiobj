package multiobj;

import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.IntSummaryStatistics;
import java.util.Random;

/**
 * Problem holds the details of the test problem.
 *
 * The context of the problem is assigning patients to primary care providers
 * (physicians or clinics) within a health care system. The constraints are:
 *   - every patient must be assigned to exactly one provider;
 *   - the number of patients assigned to a provider cannot exceed its capacity;
 *   - no patient can be required to travel more than a specified distance.
 * The criteria are:
 *   - average distance traveled by a patient (min);
 *   - maximum distance traveled by a patient (min);
 *   - maximum fraction of capacity used by any provider (min); and
 *   - minimum fraction of capacity used by any provider (max).
 * The criteria are put into three priority levels, with highest priority
 * going to average distance, second priority to maximum distance, and lowest
 * priority to an equally weighted sum of minimum and maximum fraction
 * utilization.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Problem {
  private static final double MAXDISTANCE = 100;  // maximum distance of any
                                                  // provider/patient pair
  private static final double MINCAP = 0.7;       // minimum fraction of average
                                                  // capacity at any one
                                                  // provider
  private static final String DFMT = "%.3f";      // format for doubles

  private final int nPatients;                    // number of patients
  private final int nProviders;                   // number of providers
  private final double[][] distance;              // distance of patient (row)
                                                  // to provider (column)
  private final int[] capacity;                   // provider capacity
  private final double maxDistance;               // maximum allowable distance
  private int solo;                               // # of patients with only
                                                  // one eligible provider

  /**
   * Constructor generating a random problem instance.
   * @param patients the number of patients
   * @param providers the number of providers
   * @param load ratio of patients to aggregate provider capacity
   * @param maxD maximum assignable distance (as a fraction of MAXDISTANCE)
   * @param rng a random number generator to use
   */
  public Problem(final int patients, final int providers,
                 final double load, final double maxD, final Random rng) {
    nPatients = patients;
    nProviders = providers;
    maxDistance = maxD * MAXDISTANCE;
    // Generate a random distance matrix.
    distance = new double[nPatients][nProviders];
    for (int i = 0; i < nPatients; i++) {
      double minD = Double.MAX_VALUE;  // distance to nearest provider
      for (int j = 0; j < nProviders; j++) {
        distance[i][j] = MAXDISTANCE * rng.nextDouble();
        minD = Math.min(minD, distance[i][j]);
      }
      // If none of the providers are within the maximum allowable distance,
      // tweak the distance to one of them.
      if (minD > maxDistance) {
        int j0 = rng.nextInt(nProviders);
        distance[i][j0] = maxD;
        solo += 1;
      }
    }
    // Set total provider capacity to approximate the desired load factor.
    int totalCapacity = (int) Math.round(nPatients / load);
    // Set the initial capacity of each provider to the specified minimum.
    int minCap = (int) Math.floor(totalCapacity * MINCAP / nProviders);
    capacity = new int[nProviders];
    Arrays.fill(capacity, minCap);
    totalCapacity -= nProviders * minCap;
    // Allocate any remaining capacity randomly.
    for (int i = 0; i < totalCapacity; i++) {
      int j = rng.nextInt(nProviders);
      capacity[j] += 1;
    }
  }

  /**
   * Generates a text summary of the problem.
   * @return the problem summary
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("Problem characteristics:\n");
    sb.append("There are ").append(nProviders).append(" providers and ")
      .append(nPatients).append(" patients.\n");
    DoubleSummaryStatistics dstat =
      Arrays.stream(distance)
            .flatMapToDouble((x) -> Arrays.stream(x))
            .summaryStatistics();
    sb.append("Distances range from ")
      .append(format(dstat.getMin())).append(" to ")
      .append(format(dstat.getMax()))
      .append(" with a maximum allowed distance of ")
      .append(format(maxDistance)).append(".\n");
    if (solo > 0) {
      if (solo == 1) {
        sb.append("One patient is ");
      } else {
        sb.append(solo).append(" patients are ");
      }
      sb.append("limited to a single provider due to distance.\n");
    }
    IntSummaryStatistics istat = Arrays.stream(capacity).summaryStatistics();
    sb.append("Provider capacities range from ").append(istat.getMin())
      .append(" to ").append(istat.getMax()).append(" with an average of ")
      .append(format(istat.getAverage())).append(".\n");
    return sb.toString();
  }

  /**
   * Gets the number of patients.
   * @return the number of patients
   */
  public int getNPatients() {
    return nPatients;
  }

  /**
   * Gets the number of providers.
   * @return the number of providers.
   */
  public int getNProviders() {
    return nProviders;
  }

  /**
   * Gets the maximum allowable assignment distance.
   * @return the maximum assignment distance
   */
  public double getMaxDistance() {
    return maxDistance;
  }

  /**
   * Gets the distance between a patient and a provider.
   * @param patient the patient
   * @param provider the provider
   * @return the distance between them
   */
  public double getDistance(final int patient, final int provider) {
    return distance[patient][provider];
  }

  /**
   * Gets the capacity of a provider.
   * @param provider the provider
   * @return the provider's capacity
   */
  public int getCapacity(final int provider) {
    return capacity[provider];
  }

  /**
   * Gets the distances from a patient to all providers.
   * @param patient the patient
   * @return the vector of distances to providers
   */
  public double[] getDistances(final int patient) {
    return distance[patient];
  }

  /**
   * Formats a double for printing.
   * @param x the double
   * @return the formatted value
   */
  public static String format(final double x) {
    return String.format(DFMT, x);
  }
}
