package multiobj;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;
import java.util.Random;

/**
 * MultiObj demonstrates the new (as of CPLEX 12.9) support for multiple
 * objectives in CPLEX.
 *
 * The demonstration problem assigns patients to primary care physicians in
 * a health care network. Details of the problem are explained in the Problem
 * class.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MultiObj {

  /** Dummy constructor. */
  private MultiObj() { }

  /**
   * Runs the demonstration.
   * @param args the command line arguments (ignored)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Set problem dimensions.
    int nPatients = 100;
    int nProviders = 5;
    double loadFactor = 0.7;  // system is 70% loaded
    double maxD = 0.6;        // maximum allowed patient travel distance is
                              // 60% of longest assignment distance
    long seed = 814;          // random number seed
    // Generate a problem.
    Problem problem = new Problem(nPatients, nProviders, loadFactor, maxD,
                                  new Random(seed));
    // Summarize the problem.
    System.out.println(problem);
    // Build and solve a MILP model.
    Objective[] objective = Objective.values();
    double tl = 30;  // time limit in seconds
    try (MILP model = new MILP(problem)) {
      // Find the utopia point.
      double[] utopia = model.getUtopia();
      System.out.println("Utopia:");
      for (int i = 0; i < utopia.length; i++) {
        String s = objective[i].isMinimized() ? "Minimum" : "Maximum";
        System.out.println(s + " value of " + objective[i] + " = "
                             + Problem.format(utopia[i]) + ".");
      }
      // Solve the multiobjective version.
      System.out.println("\n===== Solving the multicriterion version =====");
      IloCplex.Status status = model.solve(tl, null, null);
      System.out.println("\n==============================================");
      System.out.println("Solver status = " + status + ".");
      for (int i = 0; i < 3; i++) {
        System.out.println("Objective " + i + ": " + model.summary(i));
      }
      double[] result = model.getResults();
      for (int i = 0; i < result.length; i++) {
        System.out.println("Final value of " + objective[i] + " = "
                           + Problem.format(result[i]) + ".");
      }
      // Allow some wiggle room in the first two objectives.
      System.out.println("\n===== Solving with flexibility =====");
      model.setLogOutput(2);  // show the log output
      status = model.solve(tl, null, new double[] {0.1, 0.05, 0, 0});
      System.out.println("\n====================================");
      System.out.println("Solver status = " + status + ".");
      for (int i = 0; i < 3; i++) {
        System.out.println("Objective " + i + ": " + model.summary(i));
      }
      result = model.getResults();
      for (int i = 0; i < result.length; i++) {
        System.out.println("Final value of " + objective[i] + " = "
                           + Problem.format(result[i]) + ".");
      }
    } catch (IloException ex) {
      System.err.println(ex);
    }
  }

}
