# Multiobjective Optimization in CPLEX #

### What is this repository for? ###

This code demonstrates how to solve a multiobjective integer program in CPLEX 12.9 or later.

The problem is to assign patients to healthcare providers. Every patient must be assigned to exactly one provider. Providers have capacity limits (expressed as patient counts, where all patients are considered equal with regard to capacity). A matrix of patient-provider distances is given, and there is an upper limit on how far a patient can be required to travel.

Four criteria are specified:

1. the average distance traveled by patients;
2. the maximum distance traveled by any patient;
3. the maximum utilization of any provider (expressed as a fraction of the provider's capacity); and
4. the minimum utilization of any provider (expressed as a fraction of the provider's capacity).

The first three criteria are to be minimized and the fourth is to be maximized. To demonstrate CPLEX's ability to handle both preemptive priorities (lexicographic ordering) for criteria and weighted combinations of criteria, we assign average distance the highest priority, maximum distance the second highest priority and the two utilization criteria the lowest priority. Within the lowest priority, we assign the maximum utilization a weight of 1 and the minimum utilization a weight of -1. The latter weight is negative because we want to maximize the criterion in what is otherwise a minimization problem.

A fuller discussion of the example can be found in the blog posts ["Multiobjective Optimization"](https://orinanobworld.blogspot.com/2020/08/multiobjective-optimization.html) and ["Multiobjective Optimization in CPLEX"](https://orinanobworld.blogspot.com/2020/08/multiobjective-optimization-in-cplex.html).

The code was developed using CPLEX 12.10.

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

